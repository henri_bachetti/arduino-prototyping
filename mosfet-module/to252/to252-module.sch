EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:my-transistors
LIBS:to252-module-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_01X03 P1
U 1 1 5ED4F0C3
P 4700 4500
F 0 "P1" H 4700 4700 50  0000 C CNN
F 1 "GDS" V 4800 4500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_1x03" H 4700 4500 50  0001 C CNN
F 3 "" H 4700 4500 50  0000 C CNN
	1    4700 4500
	0    1    1    0   
$EndComp
$Comp
L R R1
U 1 1 5ED4F327
P 4600 4050
F 0 "R1" V 4500 4050 50  0000 C CNN
F 1 "220" V 4600 4050 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4530 4050 50  0001 C CNN
F 3 "" H 4600 4050 50  0000 C CNN
	1    4600 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3500 4600 3900
Wire Wire Line
	4600 3500 4900 3500
$Comp
L R R2
U 1 1 5ED4F3DE
P 4950 3800
F 0 "R2" V 5030 3800 50  0000 C CNN
F 1 "100K" V 4950 3800 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 4880 3800 50  0001 C CNN
F 3 "" H 4950 3800 50  0000 C CNN
	1    4950 3800
	0    1    1    0   
$EndComp
Wire Wire Line
	5100 3800 5200 3800
Wire Wire Line
	5200 3650 5200 4000
Wire Wire Line
	4800 3800 4600 3800
Connection ~ 4600 3800
Wire Wire Line
	4600 4300 4600 4200
Wire Wire Line
	5200 3250 5200 3100
Wire Wire Line
	5200 3100 4700 3100
Wire Wire Line
	4700 3100 4700 4300
Wire Wire Line
	4800 4300 4800 4000
Wire Wire Line
	4800 4000 5200 4000
Connection ~ 5200 3800
$Comp
L AOD409 Q1
U 1 1 5ED4FA70
P 5100 3450
F 0 "Q1" H 5350 3525 50  0000 L CNN
F 1 "AOD409" H 5350 3450 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:TO-252-2Lead" H 5350 3375 50  0001 L CIN
F 3 "" H 5100 3450 50  0000 L CNN
	1    5100 3450
	1    0    0    -1  
$EndComp
$EndSCHEMATC
